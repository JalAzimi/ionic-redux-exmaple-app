import { Component, Input } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { IAppState, Counter } from '../../app/app.module';

@Component({
  selector: 'counter',
  templateUrl: 'counter.html'
})
export class CounterComponent {

  @Input() id: number;
  @Input() count: number;
  
  constructor(private store: NgRedux<IAppState>) {}

  increment() {
    this.store.dispatch({type: 'INCREMENT', payload: this.id});
  }

  decrement() {
    this.store.dispatch({type: 'DECREMENT', payload: this.id});
  }

  removeCounter() {
    this.store.dispatch({type: 'REMOVE_COUNTER', payload: this.id});
  }

}
