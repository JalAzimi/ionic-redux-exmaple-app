import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { IAppState, Counter } from '../../app/app.module';
import { addCounterThunk } from '../../app/thunks';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @select(['counters']) counters$: Observable<Counter[]>;

  constructor(private store: NgRedux<IAppState>) {}

  addCounter() {
    this.store.dispatch(addCounterThunk());
  }
}
