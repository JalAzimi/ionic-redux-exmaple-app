import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import thunk from 'redux-thunk';

import { rootReducer } from './reducers';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CounterComponent } from '../components/counter/counter';

export interface Action {
  type: string;
  payload?: any;
}

export interface IAppState {
  lastId: number;
  counters: Counter[];
};

export interface Counter {
  id: number;
  count: number;
};

export const INITIAL_STATE: IAppState = {
  lastId: -1,
  counters: []
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CounterComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgReduxModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
  constructor(
    private ngRedux: NgRedux<IAppState>,
    private devTools: DevToolsExtension
  ) {
    ngRedux.configureStore(
      rootReducer,
      INITIAL_STATE,
      [thunk],
      [devTools.enhancer()]
    )
  }
}
