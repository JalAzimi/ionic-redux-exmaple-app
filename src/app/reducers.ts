import { IAppState, Action, Counter } from './app.module';


export const countersReducer = (state: Counter[], action: Action) => {
  switch (action.type) {
    case 'ADD_COUNTER':
      return [
        ...state,
        {id: action.payload, count: 0}];

    case 'REMOVE_COUNTER':
      return state.filter(c => c.id !== action.payload);

    case 'INCREMENT':
      return state.map(c => {
        if (c.id === action.payload) {
          return {...c, count: c.count + 1};
        }
        return c;
      })

    case 'DECREMENT':
      return state.map(c => {
        if (c.id === action.payload) {
          return {...c, count: c.count - 1};
        }
        return c;
      })

    default:
      return state;
  }
}

export const lastIdReducer = (state: number, action: Action) => {
  switch (action.type) {
    case 'ADD_COUNTER':
      return action.payload
        
    default:
      return state;
  }
}

export const rootReducer = (state: IAppState, action: Action) => {
  return {
    lastId: lastIdReducer(state.lastId, action),
    counters: countersReducer(state.counters, action)
  }
}
