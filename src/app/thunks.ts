import { Action } from './app.module';

export const addCounterThunk = () => (dispatch, getState) => {
  let lastId = getState().lastId;
  dispatch({type: 'ADD_COUNTER', payload: lastId + 1});
}